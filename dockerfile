FROM openjdk:8-alpine
COPY build/libs/RewardCentralService-0.0.1-SNAPSHOT.jar rewardcentral-service.jar
CMD ["java", "-jar", "rewardcentral-service.jar"]

