package com.quentingenet.rewardcentralservice.controller;

import com.quentingenet.rewardcentralservice.service.RewardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class RewardCentralController {

  private final Logger logger = LoggerFactory.getLogger(RewardCentralController.class);

  @Autowired
  private RewardService rewardService;

  @GetMapping("/attractionRewardPoints")
  public int getAttractionRewardPoints(@RequestParam("attractionId") UUID attractionId, @RequestParam("userId") UUID userId) {
    logger.info("GET /attractionRewardPoints?attractionId={}?userId={}", attractionId, userId);
    return rewardService.getAttractionRewardPoints(attractionId,userId);
  }
}
