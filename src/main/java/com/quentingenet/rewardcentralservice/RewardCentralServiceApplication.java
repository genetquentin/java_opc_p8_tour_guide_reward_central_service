package com.quentingenet.rewardcentralservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import rewardCentral.RewardCentral;

@SpringBootApplication
public class RewardCentralServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(RewardCentralServiceApplication.class, args);
  }

  @Bean
  public RewardCentral getRewardCentral() {
    return new RewardCentral();
  }

}
